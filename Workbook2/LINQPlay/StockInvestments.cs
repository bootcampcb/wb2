﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace LINQPlay
{
    internal class StockInvestments
    {
        public string TicketSymbol { get; set; }
        public string StockName { get; set; }
        public decimal TotalValue { get; set; }

        public StockInvestments(string ticketSymbol, string stockName, decimal totalValue)
        {
            TicketSymbol = ticketSymbol;
            StockName = stockName;
            TotalValue = totalValue;
        }
        public static List<StockInvestments> GetStockInvestments(List<Stock> stockList)
        {
            List<StockInvestments> stockInvestmentsList = new List<StockInvestments>();
            foreach (Stock s in stockList)
            {
                stockInvestmentsList.Add(new StockInvestments(s.TickerSymbol, s.ShareName, (s.SharePrice * s.UnitsOwned)));
            }
            return stockInvestmentsList;
        }
    }
}
