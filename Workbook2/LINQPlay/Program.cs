﻿// LINQ Play
using LINQPlay;
using System;

List<Stock> stockList = new List<Stock> {
    new Stock("CNC","Centene Corp", 63.30M, 1500),
    new Stock("FRC","First Republic Bank", 12.18M, 1000),
    new Stock("FDX","FedEx Corp", 219.66M, 100),
    new Stock("FHN","First Horizon Corp", 15.78M, 1000),
    new Stock("FSR", "Fisker Inc", 5.82M, 1000),
    new Stock("GM", "General Motors Company", 33.91M, 1000),
    new Stock("GME", "GameStop Corp", 22.92M, 1000),
    new Stock("GMED", "Globus Medical Inc", 52.73M, 100),
    new Stock("GMS", "GMS Inc", 53.95M, 1000),
    new Stock("GMRE", "Global Med REIT", 25.24M, 1000)
};

Console.WriteLine("Stock price less than $20");
var query = stockList.Where(s => s.SharePrice < 20);
foreach (Stock s in query)
{
    Console.WriteLine($"{s.TickerSymbol, 4} | {s.ShareName} | {s.SharePrice}");
}
Console.WriteLine("");
Console.WriteLine("Stock price between $20-$75");
var query1 = stockList.Where(s => s.SharePrice >= 20 && s.SharePrice <=75);
foreach (Stock s in query1)
{
    Console.WriteLine($"{s.TickerSymbol, 4} | {s.ShareName} | {s.SharePrice}");
}
Console.WriteLine("");
Console.WriteLine("Cheapest Stock");
var query2 = stockList.Where(s => s.SharePrice >= 20)
    .OrderBy(s => s.SharePrice)
    .First();
Console.WriteLine($"{query2.TickerSymbol, 4} | {query2.ShareName} | {query2.SharePrice}");
Console.WriteLine("");
Console.WriteLine("Stock investments");
List<StockInvestments> stockInvestments = StockInvestments.GetStockInvestments(stockList);

foreach (var si in stockInvestments)
{
    Console.WriteLine($"{si.TotalValue}");
}