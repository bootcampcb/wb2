﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQPlay
{
    internal class Stock
    {
        public string TickerSymbol { get; set; }
        public string ShareName { get; set;}
        public decimal SharePrice { get; set;}
        public int UnitsOwned { get; set;}
        public Stock(string tickerSymbol, string shareName, decimal sharePrice, int unitsOwned)
        {
            TickerSymbol = tickerSymbol;
            ShareName = shareName;
            SharePrice = sharePrice;
            UnitsOwned = unitsOwned;
        }
    }
}
