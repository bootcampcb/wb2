﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooAdmin
{
    public class Lion : Animal, ICarnivore
    {
        public Lion(string name, int age) : base(name, age)
        {
        }

        public override string Name { get; set; }
        public override int Age { get; set; }

        public string Hunt()
        {
            return "Lions hunt either alone or in prides. In collaborative hunts, it is usually the lioness who initiates the kill. Lions stalk their prey and, when close enough, attempt a short charge on their prey, trying either to pounce on their target or knock it over.";
        }

        public override string MakeSound()
        {
            return "Roar";
        }
    }
}
