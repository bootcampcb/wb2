﻿// Zoo Admin
using ZooAdmin;

Animal[] animals = new Animal[6];

animals[0] = new Lion("Leo", 5);
animals[1] = new Elephant("Dumbo", 10);
animals[2] = new Giraffe("Joffery", 7);

foreach  (var animal in animals)
{
    if (animal != null)
    {
        animal.DisplayInfo();

        Lion? lion = animal as Lion;
        if (lion != null)
        {
            Console.WriteLine($"A lions hunting behaviour: {lion.Hunt()}");
            Console.WriteLine("");
        }
        Elephant? elephant = animal as Elephant;
        if (elephant != null)
        {
            Console.WriteLine($"A elephants grazing behaviour: {elephant.Graze()}");
            Console.WriteLine("");
        }
        Giraffe? giraffe = animal as Giraffe;
        if (giraffe != null)
        {
            Console.WriteLine($"A giraffes grazing behaviour: {giraffe.Graze()}");
            Console.WriteLine("");
        }
    }

}