﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooAdmin
{
    public class Elephant : Animal, IHerbivore
    {
        public Elephant(string name, int age) : base(name, age)
        {
        }

        public override string Name { get; set; }
        public override int Age { get; set; }

        public string Graze()
        {
            return "The elephant uses its trunk to pull leaves and branches from trees.";
        }

        public override string MakeSound()
        {
            return "Trumpet";
        }
    }
}
