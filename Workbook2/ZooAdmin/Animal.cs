﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooAdmin
{
    public abstract class Animal
    {
        public Animal(string name, int age) 
        { 
            Name = name;
            Age = age;
        }
        public abstract string Name { get; set; }
        public abstract int Age { get; set; }

        public abstract string MakeSound();

        public void DisplayInfo() 
        { 
            Console.WriteLine($"Name: { Name}");
            Console.WriteLine($"Age: {Age}");
            Console.WriteLine($"Sound: {MakeSound()}");
        }
    }
}
