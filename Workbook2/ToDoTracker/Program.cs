﻿// To Do Tracker

using ToDoTracker;

bool tryAgain = true;
string? menuAns = "";
List<ToDoTask> taskList = new List<ToDoTask>();
taskList.Add(new ToDoTask("Back1", "Create API to providers", 3, "Mark", Convert.ToDateTime("04/21/2023")));
taskList.Add(new ToDoTask("Web1", "Create web page displaying providers", 3, "Laura", Convert.ToDateTime("04/28/2023")));
taskList.Add(new ToDoTask("Back2", "Create API to list claims for providers", 5, "James", Convert.ToDateTime("04/15/2023")));
taskList.Add(new ToDoTask("Back3", "Create API to list documents for claims", 2, "Dan", Convert.ToDateTime("04/25/2023")));
taskList.Add(new ToDoTask("Web1", "Create web page displaying claim/document info", 4, "Sean", Convert.ToDateTime("04/30/2023")));


do
{
    Console.WriteLine("Please select an option:");
    Console.WriteLine("1. Add a task");
    Console.WriteLine("2. Remove a task");
    Console.WriteLine("3. List tasks by due date");
    Console.WriteLine("4. List tasks by difficulty");
    Console.WriteLine("5. Quit the application");
    menuAns = Console.ReadLine();
    if (menuAns != "1" && menuAns != "2" && menuAns != "3" && menuAns != "4"
        && menuAns != "5")
    {
        Console.WriteLine("Invalid choice, please try again");
        continue;
    }
    else if (menuAns == "1")
    {
        Console.WriteLine("Enter task id");
        string taskId = Console.ReadLine();
        Console.WriteLine("Enter task description");
        string taskName = Console.ReadLine();
        Console.WriteLine("Difficulty:");
        int difficulty = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Assigned to:");
        string assignedToName = Console.ReadLine();
        Console.WriteLine("Enter due date");
        DateTime dueDate = Convert.ToDateTime(Console.ReadLine());

        taskList.Add(new ToDoTask(taskId, taskName, difficulty, assignedToName, dueDate));
    }
    else if (menuAns == "2")
    {
        foreach (ToDoTask task in taskList)
        {
            Console.WriteLine($"{task.ToString()}");
        }
        Console.WriteLine("Enter task id to remove");
        string taskId = Console.ReadLine();
    }
    else if (menuAns == "3")
    {
        taskList.Sort((x, y) => DateTime.Compare((DateTime)x.DueDate, (DateTime)y.DueDate));
        foreach (ToDoTask task in taskList)
        {
            Console.WriteLine($"{task.ToString()}");
        }
    }
    else if (menuAns == "4")
    {
        taskList.Sort(new DifficultySorter());
        foreach (ToDoTask task in taskList)
        {
            Console.WriteLine($"{task.ToString()}");
        }
    }
    else if (menuAns == "5")
    {
        tryAgain = false;
    }

} while (tryAgain);