﻿namespace ToDoTracker
{
    internal class ToDoTask : IComparable<ToDoTask>
    {
        public string TaskID { get; set; }
        public string TaskName { get; set; }
        public int Difficulty { get; set; }
        public string AssignedToName { get; set; }
        public DateTime DueDate { get; set; }

        public ToDoTask(string taskId, string taskName, int difficulty, string assignToName, DateTime dueDate)
        {
            TaskID = taskId;
            TaskName = taskName;
            Difficulty = difficulty;
            AssignedToName = assignToName;
            DueDate = dueDate;
        }

        public override string ToString()
        {
            var date = DateOnly.FromDateTime(DueDate);

            return $"Task ID: {TaskID} Task Name: {TaskName} Difficulty: {Difficulty} AssignedToName: {AssignedToName} Due date: {date}";
        }
        public int CompareTo(ToDoTask other)
        {
            return TaskName.CompareTo(other.AssignedToName);
        }
    }
    internal class DifficultySorter : IComparer<ToDoTask>
    {
        public int Compare(ToDoTask x, ToDoTask y)
        {
            return x.Difficulty - y.Difficulty;
        }
    }
}
