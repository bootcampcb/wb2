﻿// FileProcessing
using FileProcessing;

StreamReader? sr = null;
String directoryname = @"C:\MCS Development\FileProcessing\";
String fileName = "";

for (int i = 1; i < 4; i++)
{
    fileName = "PayrollData" + Convert.ToString(i) + ".txt";

    try
    {
        sr = new StreamReader(directoryname + fileName);
        // process file
        Console.WriteLine($"Processing {directoryname}{fileName}");
        while (sr.EndOfStream != true)
        {
            string row = sr.ReadLine();
            TimeCard? tc = TimeCard.CreateTimeCard(row);
            if (tc != null)
            {
                Console.WriteLine($"Employee: {tc.Name} Gross Pay: {tc.GetGrossPay()}");
            }
            else
            {
                Console.WriteLine($"Bad line in file: {row}");
            }
        }
        sr.Close();
    }
    catch (FileNotFoundException ex)
    {
        Console.WriteLine($"Where's your file {fileName}????");
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
    }
    finally
    {
        if (sr != null) sr.Close();
    }
}
