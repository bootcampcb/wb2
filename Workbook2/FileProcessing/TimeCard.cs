﻿using System;

namespace FileProcessing
{
    public class TimeCard
    {
        public String Name { get; set; }
        public decimal HoursWorked { get; set; }
        public decimal PayRate { get; set; }

        public TimeCard(string name, decimal hoursWorked, decimal payRate)
        {
            Name = name;
            HoursWorked = hoursWorked;
            PayRate = payRate;
        }

        public static TimeCard CreateTimeCard(string data)
        {
            string[] fields = data.Split('|');
            try
            {
                TimeCard t = new TimeCard(fields[0], Convert.ToDecimal(fields[1]), Convert.ToDecimal(fields[2]));
                return t;
            }
            catch (IndexOutOfRangeException ex) 
            { 
                Console.WriteLine(ex.Message);
                return null;
            }

        }
        public virtual decimal GetGrossPay()
        {
            if (HoursWorked <= 40)
            {
                return PayRate * HoursWorked;
            }
            else
            {
                decimal overTime = HoursWorked - 40; 
                return (PayRate * 40) + (PayRate * 1.5M * overTime);
            }

        }
    }
}
