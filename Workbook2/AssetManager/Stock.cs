﻿namespace AssetManager
{
    public class Stock : Asset
    {
        public string StockTicker { get; set; } = "";
        public decimal CurrentSharePrice { get; set; } = 0;
        public int NumberOfShares { get; set; } = 0;

        public Stock(int assetId, string description, DateTime dateAcquired, decimal originalCost, string stockTicker, decimal currentSharePrice, int numberOfShares) 
            : base(assetId, description, dateAcquired, originalCost)
        {
            StockTicker = stockTicker;
            CurrentSharePrice = currentSharePrice;
            NumberOfShares = numberOfShares;
        }
        public override decimal GetValue()
        {
            return CurrentSharePrice * NumberOfShares;
        }
    }
}

