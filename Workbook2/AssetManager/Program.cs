﻿using AssetManager;
// Asset Manager
Asset[] assets = new Asset[6];

Stock s1 = new Stock(1, "Microsoft", DateTime.Now, 27054, "MSFT", 270.54M, 100);
Stock s2 = new Stock(2, "Centene Corp", DateTime.Now, 6520, "CNC", 65.2M, 100);
Stock s3 = new Stock(3, "Ford", DateTime.Now, 11760, "F", 11.76M, 1000);

Car c1 = new Car(4, "Porche 944", DateTime.Parse("05/01/1986"), 10000, 1986, 76000);
Car c2 = new Car(5, "AMC Pacer", DateTime.Parse("05/01/1976"), 3000, 1976, 100000);
Car c3 = new Car(6, "Chevy Silverado 1500", DateTime.Parse("11/01/2017"), 50000, 2017, 10000);

assets[0] = s1;
assets[1] = s2;
assets[2] = s3;
assets[3] = c1;
assets[4] = c2;
assets[5] = c3;
Console.WriteLine("All assets:");
foreach (var asset in assets)
{

    Console.WriteLine($"Description: {asset.Description} Date Acquired: {asset.DateAcquired} Current Value {asset.GetValue()}");
}
Console.WriteLine("");
Console.WriteLine("All cars:");
foreach (var asset in assets)
{
    Car? c = asset as Car;
    if (c != null)
    {
        Console.WriteLine($"Description: {c.Description} Year: {c.Modelyear} Odometer: {c.CurrentOdometerReading} Date Acquired: {c.DateAcquired} Current Value {c.GetValue()}");
    }
}
Console.WriteLine("");
Console.WriteLine("All stocks:");
foreach (var asset in assets)
{
    Stock? s = asset as Stock;
    if (s != null)
    {
        Console.WriteLine($"Description: {s.Description} Date Acquired: {s.DateAcquired} Ticker Symbol: {s.StockTicker} Current share price: {s.CurrentSharePrice} Number of shares: {s.NumberOfShares}  Current Value {s.GetValue()}");
    }
}

Console.ReadLine();