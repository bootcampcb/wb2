﻿namespace AssetManager
{
    public class Asset
    {
        public int AssetId { get; set; } = 0;
        public string Description { get; set; } = "";
        public DateTime? DateAcquired { get; set; } = null;
        public decimal OriginalCost { get; set; } = 0;
        public Asset(int assetId, string description, DateTime dateAcquired, decimal originalCost)
        {
            AssetId = assetId;
            Description = description;
            DateAcquired = dateAcquired;
            OriginalCost = originalCost;
        }

        public virtual decimal GetValue()
        {
            return OriginalCost;
        }
    }
}

