﻿namespace AssetManager
{
    public class Car : Asset
    {
        public int Modelyear { get; set; } = 0;
        public decimal CurrentOdometerReading { get; set; } = 0;


        public Car(int assetId, string description, DateTime dateAcquired, decimal originalCost, int modelYear, int currentOdometerReading)
            : base(assetId, description, dateAcquired, originalCost)
        {
            Modelyear = modelYear;
            CurrentOdometerReading = currentOdometerReading;
        }
        public override decimal GetValue()
        {
            int age = DateTime.Now.Year - Modelyear;
            decimal discount = 0;
            if (age < 7)
            {
                discount = 1 - ((CurrentOdometerReading / 5000) * (decimal).02);
                if (discount < (decimal)0.9)
                {
                    return OriginalCost * discount;
                }
                else
                {

                    return OriginalCost * (decimal).90;

                }
            }
            else
            {
                if (CurrentOdometerReading < 100000)
                {
                    return OriginalCost * (decimal).7;
                }
                else
                {
                    return OriginalCost * (decimal).9;
                }
            }

        }
    }
}

