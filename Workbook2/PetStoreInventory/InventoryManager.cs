﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PetStoreInventory
{
    internal class InventoryManager
    {
        private List<InventoryItem> inventory;
        private Dictionary<InventoryItem, int> cart;
        public InventoryManager()
        {
            inventory = new List<InventoryItem>();
            cart = new Dictionary<InventoryItem, int>();
        }

        public void LoadInventory(string filePath)
        {
            try
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    string line;
                    int lineCnt = 0;
                    while ((line = reader.ReadLine()) != null)
                    {
   //                     Console.WriteLine($"Reading Line {lineCnt} - {line}");
                        string[] fields = line.Split('|');
                        string itemType = fields[0].Substring(0,1);
                        string id = fields[0];
                        string name = fields[1];
                        string description = fields[2];
                        decimal price = decimal.Parse(fields[3]);
                        int quantity = int.Parse(fields[4]);

                        switch (itemType.ToUpper())
                        {
                            case "I":
                                InventoryItem inventoryItem = new InventoryItem(id, name, description, price, quantity);
                                inventory.Add(inventoryItem);
                                break;
                            case "F":
                                string brand = fields[5];
                                string weight = fields[6];
                                FoodItem foodItem = new FoodItem(id, name, description, price, quantity, brand, weight);
                                inventory.Add(foodItem);
                                break;

                            case "T":
                                string material = fields[5];
                                string size = fields[6];
                                ToyItem toyItem = new ToyItem(id, name, description, price, quantity, material, size);
                                inventory.Add(toyItem);
                                break;

                            case "C":
                                double length = double.Parse(fields[5]);
                                double width = double.Parse(fields[6]);
                                double height = double.Parse(fields[7]);
                                CageItem cageItem = new CageItem(id, name, description, price, quantity, length, width, height);
                                inventory.Add(cageItem);
                                break;

                            case "A":
                                string category = fields[5];
                                AccessoryItem accessoryItem = new AccessoryItem(id, name, description, price, quantity, category);
                                inventory.Add(accessoryItem);
                                break;

                            default:
                                Console.WriteLine($"Invalid item type {itemType} - Line {lineCnt}.");
                                break;
                        }
                        lineCnt++;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading inventory: {ex.Message}");
            }
        }
        public void DisplayCart()
        {

            if (cart == null)
            {
                Console.WriteLine("Cart is empty!");
            }
            else
            {
                Console.WriteLine("Inventory:");
                foreach(var item in cart) 
                {
                    Console.WriteLine($"Qty: {item.Value} Id: {item.Key.Id} - {item.Key.Name}");
                }
            }
        }
        public string CheckOut(string inventoryDir)
        {
            DateTime d = DateTime.Now;
            decimal cartTotal = cart.Sum(x => x.Key.Price);
            Console.WriteLine($"Thank you for wanting to purchase! Your total is {cartTotal}");
            Console.WriteLine("Please enter your name");
            string name = Console.ReadLine();
            string invoiceFileName = $"{inventoryDir}{name} - {d.Year}{d.Month:D2}{d.Day:D2}{d.Hour:D2}{d.Minute}.txt"; ;
            using (StreamWriter outputFile = new StreamWriter(invoiceFileName))
            {
                outputFile.WriteLine($"{d:MMMM d, yyyy} at {d:h:mm:ss} - Total Purchase: {cartTotal:C2}");
                outputFile.WriteLine("");
                outputFile.WriteLine($"{"ID",-20} {"Name",-19} {"Description",-55} {"Price",- 10:C2} {"Qty",-5}");
                foreach (var item in cart)
                {
                    outputFile.Write($"{item.Key.Id,-20} {item.Key.Name,-19} {item.Key.Description,-55} {item.Key.Price, -10:C2} {item.Value,-5} ");

                    if (item is FoodItem)
                    {
                        FoodItem foodItem = (FoodItem)item.Key;
                        outputFile.WriteLine($"{foodItem.Brand,-20} {foodItem.Weight,-20}");
                    }
                    else if (item is ToyItem)
                    {
                        ToyItem toyItem = (ToyItem)item.Key;
                        outputFile.WriteLine($"{toyItem.Material,-20} {toyItem.Size,-20:F2}");
                    }
                    else if (item is CageItem)
                    {
                        CageItem cageItem = (CageItem)item.Key;
                        outputFile.WriteLine($"{cageItem.Length,-6:F2}/{cageItem.Width,-6:F2}/{cageItem.Height,-6:F2}");
                    }
                    else if (item is AccessoryItem)
                    {
                        AccessoryItem accessoryItem = (AccessoryItem)item.Key;
                        outputFile.WriteLine($"{accessoryItem.Category,-20}");
                    }
                    else
                    {
                        outputFile.WriteLine();
                    }
                }
            }
            cart.Clear();
            return invoiceFileName;
        }
        public void DisplayInventory()
        {
            Console.WriteLine("Inventory:");

            Console.WriteLine("{0,-14} {1,-20} {2,-19}", "Type", "ID", "Name");
            //Console.WriteLine("{0,-14} {1,-20} {2,-19} {3,-55} {4,-10} {5,-5} {6,-20}", "", "", "", "", "", "", "Length/Width/Height");
            foreach (InventoryItem item in inventory)
            {
                //Console.Write("{0,-14} {1,-20} {2,-19} {3,-55} {4,-10:C2} {5,-5} ", item.GetType().Name, item.Id, item.Name, item.Description, item.Price, item.Quantity);
                Console.Write("{0,-14} {1,-20} {2,-19} {3, -6}", item.GetType().Name, item.Id, item.Name, item.Quantity);

                //if (item is FoodItem)
                //{
                //    FoodItem foodItem = (FoodItem)item;
                //    Console.WriteLine("{0,-20} {1,-20}", foodItem.Brand, foodItem.Weight);
                //}
                //else if (item is ToyItem)
                //{
                //    ToyItem toyItem = (ToyItem)item;
                //    Console.WriteLine("{0,-20} {1,-20:F2}", toyItem.Material, toyItem.Size);
                //}
                //else if (item is CageItem)
                //{
                //    CageItem cageItem = (CageItem)item;
                //    Console.WriteLine("{0,-6:F2}/{1,-6:F2}/{2,-6:F2}", cageItem.Length, cageItem.Width, cageItem.Height);
                //}
                //else if (item is AccessoryItem)
                //{
                //    AccessoryItem accessoryItem = (AccessoryItem)item;
                //    Console.WriteLine("{0,-20}", accessoryItem.Category);
                //}
                //else
                //{
                Console.WriteLine();
                //}
            }
        }
        public void AddInventoryItem(InventoryItem item)
        {
            inventory.Add(item);
        }
        public void RemoveInventoryItem(string itemId)
        {
            InventoryItem itemToRemove = inventory.FirstOrDefault(item => item.Id == itemId);

            if (itemToRemove != null)
            {
                Console.WriteLine($"{itemId}-{itemToRemove.Name} was removed");
                inventory.Remove(itemToRemove);
            }
            else
            {
                Console.WriteLine($"Item with ID '{itemId}' not found.");
            }
        }

        public void UpdateInventoryItem(string itemId, InventoryItem newItem)
        {
            InventoryItem itemToUpdate = inventory.FirstOrDefault(item => item.Id == itemId);

            if (itemToUpdate != null)
            {
                itemToUpdate.Name = newItem.Name;
                itemToUpdate.Description = newItem.Description;
                itemToUpdate.Price = newItem.Price;
                itemToUpdate.Quantity = newItem.Quantity;

                switch (itemToUpdate)
                {
                    case FoodItem foodItem:
                        foodItem.Brand = ((FoodItem)newItem).Brand;
                        foodItem.Weight = ((FoodItem)newItem).Weight;
                        break;

                    case ToyItem toyItem:
                        toyItem.Material = ((ToyItem)newItem).Material;
                        toyItem.Size = ((ToyItem)newItem).Size;
                        break;

                    case CageItem cageItem:
                        cageItem.Length = ((CageItem)newItem).Length;
                        cageItem.Width = ((CageItem)newItem).Width;
                        cageItem.Height = ((CageItem)newItem).Height;
                        break;

                    case AccessoryItem accessoryItem:
                        accessoryItem.Category = ((AccessoryItem)newItem).Category;
                        break;
                }
            }
            else
            {
                Console.WriteLine($"Item with ID '{itemId}' not found.");
            }
        }

        public void DisplayInventoryItems()
        {
            Console.WriteLine("ID\tName\t\tPrice\tQuantity");

            foreach (InventoryItem item in inventory)
            {
                Console.WriteLine($"{item.Id}\t{item.Name,-15}\t${item.Price:F2}\t{item.Quantity}");
            }
        }

        public InventoryItem GetItemById(string itemId)
        {
            InventoryItem itemToFind = inventory.FirstOrDefault(item => item.Id == itemId);

            if (itemToFind == null)
            {
                Console.WriteLine($"Item with ID '{itemId}' not found.");
            }

            return itemToFind;
        }
        public InventoryItem AddToCart(InventoryItem pItem, int qty)
        {
            InventoryItem itemToFind = inventory.FirstOrDefault(item => item.Id == pItem.Id);

            if (itemToFind == null)
            {
                Console.WriteLine($"Item with ID '{pItem.Id}' not found.");
            }
            else if (itemToFind.Quantity >= qty)
            {
                //Console.WriteLine($"{itemToFind.GetType().Name}");
                cart.Add(itemToFind, qty);
                inventory.FirstOrDefault(item => item.Id == pItem.Id).Quantity -= qty;

            }
            else
            {
                Console.WriteLine("Insufficient Stock");
            }

            return itemToFind;
        }
        public string GetNextItemId(string itemIdPrefix)
        {
             string newItemId = "";
             InventoryItem itemToFind = inventory.Last(item => item.Id.StartsWith(itemIdPrefix));

            if (itemToFind == null)
            {
                Console.WriteLine($"Items of type '{itemIdPrefix}' not found.");
            }
            else
            {
                int idNum = int.Parse(itemToFind.Id.Substring(1));
                idNum++;
                newItemId = itemIdPrefix + idNum.ToString("0000");
            }

            return newItemId;
        }
        public List<InventoryItem> SearchDescription(string searchString)
        {
            var query = inventory.Where(item => (item.Description.ToLower().Contains(searchString))).ToList();
            return query;
        }
        public List<InventoryItem> SearchPriceRange(decimal lowerPriceRange, decimal upperPriceRange)
        {
            var query = inventory.Where(item => (item.Price >= lowerPriceRange && item.Price <= upperPriceRange)).ToList();
            return query;
        }
        public List<InventoryItem> SearchType(string searchString)
        {
            var query = inventory.Where(item => (item.GetType().Name.ToLower().Contains(searchString))).ToList();
            return query;
        }
    }
}

