﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using PetStoreInventory;

//List<InventoryItem> inventory = new List<InventoryItem>();
string inventoryDir = @"C:\MCS Development\PetStore\";
string inventoryFile = $"{inventoryDir}inventory.txt";
InventoryManager manager = new InventoryManager();
manager.LoadInventory(inventoryFile);

bool exit = false;
static bool ToBoolFuzzy(string stringVal)
{
    string normalizedString = (stringVal?.Trim() ?? "false").ToLowerInvariant();
    bool result = (normalizedString.StartsWith("y")
        || normalizedString.StartsWith("t")
        || normalizedString.StartsWith("1"));
    return result;
}

do
{
    Console.WriteLine("Menu:");
    Console.WriteLine("1. Look up item by ID");
    Console.WriteLine("2. Find items that match type (derived class name)");
    Console.WriteLine("3. Find items that fall within a price range");
    Console.WriteLine("4. Find items that match a keyword in the description");
    Console.WriteLine("5. View all items");
    Console.WriteLine("6. Add item to inventory");
    Console.WriteLine("7. Remove item from inventory");
    Console.WriteLine("8. Show cart");
    Console.WriteLine("9. Check out");
    Console.WriteLine("10. Exit");

    Console.Write("Enter selection: ");
    string input = Console.ReadLine();

    switch (input)
    {
        case "1":
            Console.Write("Enter ID: ");
            string id = Console.ReadLine();

            InventoryItem item = manager.GetItemById(id);
            if (item != null)
            {
                Console.WriteLine(item);
                Console.WriteLine("");
                Console.WriteLine("Would you like to purchase?");
                bool ans = ToBoolFuzzy(Console.ReadLine());
                if (ans)
                {
                    Console.WriteLine("How many?");
                    int qty = int.Parse(Console.ReadLine());
                    manager.AddToCart(item, qty);
                }
;
            }
            else
            {
                Console.WriteLine("Item not found.");
            }
            break;
        case "2":
            Console.WriteLine("Search type for:");
            string searchTypeString = Console.ReadLine();
            List<InventoryItem> typeResults = manager.SearchType(searchTypeString.ToLower());
            Console.WriteLine($"{typeResults.Count} items returned");
            foreach (InventoryItem result in typeResults)
            {
                //Console.WriteLine($"{result.Id} - {result.Name}: {result.Description} for {result.Price}");
                Console.WriteLine(result);
                Console.WriteLine("");
                Console.WriteLine("Would you like to purchase?");
                bool ans = ToBoolFuzzy(Console.ReadLine());
                if (ans)
                {
                    Console.WriteLine("How many?");
                    int qty = int.Parse(Console.ReadLine());
                    manager.AddToCart(result, qty);
                }
            }
            break;
        case "3":
            Console.WriteLine("Upper price range:");
            decimal upperPriceRange = decimal.Parse(Console.ReadLine());
            Console.WriteLine("Lower price range:");
            decimal lowerPriceRange = decimal.Parse(Console.ReadLine());
            List<InventoryItem> searchResults = manager.SearchPriceRange(lowerPriceRange, upperPriceRange);
            Console.WriteLine($"{searchResults.Count} items returned");
            foreach (InventoryItem result in searchResults)
            {
                //Console.WriteLine($"{result.Id} - {result.Name}: {result.Description} for {result.Price}");
                Console.WriteLine(result);
                Console.WriteLine("");
                Console.WriteLine("Would you like to purchase?");
                bool ans = ToBoolFuzzy(Console.ReadLine());
                if (ans)
                {
                    Console.WriteLine("How many?");
                    int qty = int.Parse(Console.ReadLine());
                    manager.AddToCart(result, qty);
                }
            }
            break;
        case "4":
            Console.WriteLine("Search descripton for:");
            string searchString = Console.ReadLine();
            List<InventoryItem> priceResults = manager.SearchDescription(searchString.ToLower());
            Console.WriteLine($"{priceResults.Count} items returned");
            foreach (InventoryItem result in priceResults)
            {
                //Console.WriteLine($"{result.Id} - {result.Name}: {result.Description} for {result.Price}");
                Console.WriteLine(result);
                Console.WriteLine("");
                Console.WriteLine("Would you like to purchase?");
                bool ans = ToBoolFuzzy(Console.ReadLine());
                if (ans)
                {
                    Console.WriteLine("How many?");
                    int qty = int.Parse(Console.ReadLine());
                    manager.AddToCart(result, qty);
                }
            }
            break;
        case "5":
            manager.DisplayInventory();
            break;

        case "6":
            Console.WriteLine("Add item to inventory:");
            Console.Write("Enter item type (food/toy/cage/accessory): ");
            string itemType = Console.ReadLine();
            string ItemIdPrefix = itemType.ToUpper().Substring(0, 1);

            //Console.Write("Enter ID: ");
            string newItemId = manager.GetNextItemId(ItemIdPrefix);
            if (newItemId == null)
            {
                Console.WriteLine("Next Item Id not found");
                break;
            }
            Console.WriteLine($"Next Item Id: {newItemId}");

            Console.Write("Enter name: ");
            string newItemName = Console.ReadLine();

            Console.Write("Enter description: ");
            string newItemDesc = Console.ReadLine();

            Console.Write("Enter price: ");
            decimal newItemPrice = decimal.Parse(Console.ReadLine());

            Console.Write("Enter quantity: ");
            int newItemQty = int.Parse(Console.ReadLine());

            switch (itemType.ToLower())
            {
                case "food":
                    Console.Write("Enter brand: ");
                    string brand = Console.ReadLine();

                    Console.Write("Enter weight: ");
                    string weight = Console.ReadLine();

                    FoodItem foodItem = new FoodItem(newItemId, newItemName, newItemDesc, newItemPrice, newItemQty, brand, weight);
                    manager.AddInventoryItem(foodItem);
                    break;

                case "toy":
                    Console.Write("Enter material: ");
                    string material = Console.ReadLine();

                    Console.Write("Enter size: ");
                    string size = Console.ReadLine();

                    ToyItem toyItem = new ToyItem(newItemId, newItemName, newItemDesc, newItemPrice, newItemQty, material, size);
                    manager.AddInventoryItem(toyItem);
                    break;

                case "cage":
                    Console.Write("Enter length: ");
                    double length = double.Parse(Console.ReadLine());

                    Console.Write("Enter width: ");
                    double width = double.Parse(Console.ReadLine());

                    Console.Write("Enter height: ");
                    double height = double.Parse(Console.ReadLine());

                    CageItem cageItem = new CageItem(newItemId, newItemName, newItemDesc, newItemPrice, newItemQty, length, width, height);
                    manager.AddInventoryItem(cageItem
                                                );
                    break;

                case "accessory":
                    Console.Write("Enter category: ");
                    string category = Console.ReadLine();

                    AccessoryItem accessoryItem = new AccessoryItem(newItemId, newItemName, newItemDesc, newItemPrice, newItemQty, category);
                    manager.AddInventoryItem(accessoryItem);
                    break;

                default:
                    Console.WriteLine("Invalid item type.");
                    break;
            }
            break;
        case "7":
            Console.Write("Enter ID: ");
            string idToRemove = Console.ReadLine();
            manager.RemoveInventoryItem(idToRemove);
            break;
        case "8":
            manager.DisplayCart();
            break;
        case "9":
            string invoiceName = manager.CheckOut(inventoryDir);
            Console.WriteLine($"Please collect invoice {invoiceName} from {inventoryDir}");
            break;
        case "10":
            exit = true;
            break;

        default:
            Console.WriteLine("Invalid selection.");
            break;
    }

    Console.WriteLine();
} while (!exit);
