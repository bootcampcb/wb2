﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetStoreInventory
{
    internal class CageItem : InventoryItem
    {
        public double Length { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public CageItem(string id, string name, string description, decimal price, int quantity, double length, double width, double height) : base(id, name, description, price, quantity)
        {
            Length = length;
            Width = width;
            Height = height;
        }
        public override string ToString()
        {
            return base.ToString() + $", L: {Length}\" X W: {Width}\" X H: {Height}\"";
        }
    }
}
