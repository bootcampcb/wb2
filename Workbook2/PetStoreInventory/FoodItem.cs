﻿namespace PetStoreInventory
{
    internal class FoodItem : InventoryItem
    {
        public string Brand { get; set; }
        public string Weight { get; set; }

        public FoodItem(string id, string name, string description, decimal price, int quantity, string brand, string weight) : base(id, name, description, price, quantity)
        {
            Brand = brand;
            Weight = weight;
        }
        public override string ToString()
        {
            return base.ToString() + $", {Brand} brand, {Weight} oz";
        }
    }
}