﻿namespace PetStoreInventory
{
    internal class ToyItem : InventoryItem
    {

        public string Material { get; set; }
        public string Size { get; set; }

        public ToyItem(string id, string name, string description, decimal price, int quantity, string material, string size) : base(id, name, description, price, quantity)
        {
            Material = material;
            Size = size;
        }
        public override string ToString()
        {
            return base.ToString() + $", made of {Material}, {Size} inches";
        }
    }
}