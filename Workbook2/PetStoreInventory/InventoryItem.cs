﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetStoreInventory
{
    internal class InventoryItem
    {
        public string Id {  get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public InventoryItem(string id, string name, string description, decimal price, int quantity) 
        { 
            Id = id;
            Name = name;
            Description = description;
            Price = price;
            Quantity = quantity;
        }
        public override string ToString()
        {
            return $"{GetType().Name}: {Name} ({Id}) - {Description}, {Price:C}, {Quantity} in stock";
        }
    }
}
