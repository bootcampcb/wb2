﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetStoreInventory
{
    internal class AccessoryItem : InventoryItem
    {
        public string Category { get; set; }
        public AccessoryItem(string id, string name, string description, decimal price, int quantity, string category) : base(id, name, description, price, quantity)
        {
            Category = category;
        }
        public override string ToString()
        {
            return base.ToString() + $", Category: {Category}";
        }
    }
}
