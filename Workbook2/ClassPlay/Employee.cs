﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ClassPlay
{
    public class Employee
    {
        private static int _firstYear = DateTime.Now.Year;

        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public Int32 EmployeeId { get; set; } = 0;
        public Int32 FirstYear { get; set; } = 0;
        public string JobTitle { get; set; } = "";
        public string Dept { get; set; } = "";
        public decimal Salary { get; set; } = 0;

        public Employee(string firstName, string lastName, int employeeId, int firstYear, string jobTitle, string dept, decimal salary) 
        { 
            FirstName = firstName;
            LastName = lastName;
            EmployeeId = employeeId;
            FirstYear = firstYear;
            JobTitle = jobTitle;
            Dept = dept;
            Salary = salary;
        }   
        public Employee(string firstName, string lastName, int employeeId) : this(firstName, lastName, employeeId, _firstYear, "New Hire", "TBD", 15 * 2000)
        {
            //FirstName = firstName;
            //LastName = lastName;
            //EmployeeId = employeeId;
            //FirstYear = DateTime.Now.Year;
            //JobTitle = "New Hire";
            //Dept = "TBD";
            //Salary = 15 * 2000;
        }

        public void Display()
        {
            Console.WriteLine($"First Name: {FirstName}");
            Console.WriteLine($"Last Name: {LastName}");
            Console.WriteLine($"Job Title: {JobTitle}");
            Console.WriteLine($"Dept: {Dept}");
            Console.WriteLine($"Employee ID: {EmployeeId}");
            Console.WriteLine($"First Year: {FirstYear}");
            Console.WriteLine($"Salary: {Salary}");
        }
        public void Promote(string jobTitle, decimal newPay)
        {
            JobTitle = jobTitle;
            Salary = newPay;
        }
    }
}
