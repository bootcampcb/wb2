﻿using ClassPlay;

Employee e1 = new Employee("Chuck", "Borst", 1, 2009, "SE", "IT", 100000);
Employee e2 = new Employee("John", "Doe", 2);
//employee1.FirstName = "Chuck";
//employee1.LastName = "Borst";
//employee1.EmployeeId = 1;
//employee1.FirstYear = 2009;
//employee1.JobTitle = "Test";
//employee1.Salary = 100000;
//employee1.Dept = "IT";
//DisplayEmployee(e1 as Employee);
//DisplayEmployee(e2 as Employee);

e1.Display();
e2.Display();


e2.Promote("Supreme Leader", 1000000);
e2.Display();

Console.ReadLine();

static void DisplayEmployee(Employee e)
{
    Console.WriteLine("First Name: " + e.FirstName);
    Console.WriteLine("Last Name: " + e.LastName);
    Console.WriteLine("Dept: " + e.Dept);
    Console.WriteLine("Employee ID: " + e.EmployeeId);
    Console.WriteLine("First Year: " + e.FirstYear);
    Console.WriteLine($"Salary: {e.Salary}");
}

