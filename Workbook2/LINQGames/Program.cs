﻿// LINQ Games
using LINQGames;
using System.Xml.Linq;

List<Supplier> suppliers = new List<Supplier> {
 new Supplier(101, "ACME", "acme.com"),
 new Supplier(201, "Spring Valley", "spring-valley.com"),
};

List<Product> products = new List<Product> {
 new Product(1, "Dark Chocolate Bar", 4.99M, 10, 101),
 new Product(2, "8 oz Guacamole", 5.99M, 27, 201),
 new Product(3, "Milk Chocolate Bar", 3.99M, 16, 101),
 new Product(4, "8 pkg Chicken Tacos", 15.99M, 7, 201),
};


var questioin1Query = from s in suppliers select s;
Console.WriteLine("List Suppliers names");
foreach (Supplier s in questioin1Query)
{
    Console.WriteLine(s.Name);
}
var question2Query = from p in products
                     where p.Price <= 5
                     select p;
Console.WriteLine("");
Console.WriteLine("");Console.WriteLine("Products $5.00 or less");
foreach (Product p in question2Query)
{
    Console.WriteLine($"Name: {p.ProductName} - Price: {p.Price}");
}
var question3Query = from p in products
                     where p.QuantityOnHand >= 10
                     select p;
Console.WriteLine("");
Console.WriteLine("Products that we have 10 or more of");
foreach (Product p in question3Query)
{
    Console.WriteLine($"Name: {p.ProductName} - Qty: {p.QuantityOnHand}");
}


var question4Query = (from s in suppliers
                     select s).Last();
Console.WriteLine("");
Console.WriteLine("Info supplier 201");
Console.WriteLine($"{question4Query.SupplierId,4} | {question4Query.Name}");
var question5Query = (from p in products
                      where p.Price >= 5
                      select p).First();
Console.WriteLine("");
Console.WriteLine("First product $5 or more");
Console.WriteLine($"{question5Query.ProductId,4} | {question5Query.ProductName} | {question5Query.Price}");
var question6Query = (from p in products
                      where p.Price >= 5
                      select p).Count();
Console.WriteLine("");
Console.WriteLine("Count of products $5 or more");
Console.WriteLine($"Count: {question6Query}");
var question7Query = (from p in products
                      where p.Price <= 5
                      select p).Count();
Console.WriteLine("");
Console.WriteLine("Count of products $5 or less");
Console.WriteLine($"Count: {question7Query}");
static List<Product> GetItemsToBeReordered(List<Product> products)
{
    return (from p in products
            where p.QuantityOnHand <= 10
            select p).ToList();
}
Console.WriteLine("");
Console.WriteLine("Products needing to be reordered");
foreach(var item in GetItemsToBeReordered(products))
{
    Console.WriteLine($"{item.ProductName} has only {item.QuantityOnHand} left");
};
var question9Query = from p in products
                     join s in suppliers on p.SupplierId equals s.SupplierId
                     select new
                     {
                         p.ProductName,
                         p.Price,
                         SupplierName = s.Name
                     };
Console.WriteLine("");
Console.WriteLine("Join product and supplier");
foreach (var p in question9Query)
{
    Console.WriteLine($"Name: {p.ProductName} - Price: {p.Price} - Supplier {p.SupplierName}");
}

