﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    internal class Worker : Person
    {
        public string JobTitle { get; set; }
        public decimal Salary { get; set; }

        public Worker(string name, int age, string address, string city, string state, string zipcode, string jobTitle, decimal salary) : base(name, age, address, city, state, zipcode)
        {
            JobTitle = jobTitle;
            Salary = salary;
        }
        public override void Display()
        {
            Console.WriteLine($"W--> Name: {Name} Age: {Age} Address: {Address} {City} {State} {ZipCode}");
            Console.WriteLine($"w--> Job title: {JobTitle} - Salary: {Salary}");
        }
    }
}
