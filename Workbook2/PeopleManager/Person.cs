﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    internal class Person : IComparable<Person>
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }



        public Person(string name, int age, string address, string city, string state, string zipcode) 
        {
            Name = name;
            Age = age;
            Address = address;
            City = city;
            State = state;
            ZipCode = zipcode;
        }

        public int HaveABirthday()
        {
            Age++ ; return Age;
        }

        public void Move(string address, string city, string state, string zipcode)
        {
            Address = address;
            City = city;
            State = state;
            ZipCode = zipcode;
        }
        public virtual void Display()
        {
            Console.WriteLine($"Name: {Name} Age: {Age} Address: {Address} {City} {State} {ZipCode}");
        }

        public int CompareTo(Person other )
        {
            return Name.CompareTo( other.Name );
        }
    }
}
