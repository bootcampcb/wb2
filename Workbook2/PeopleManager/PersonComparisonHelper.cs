﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    internal class DescendingAgeSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return y.Age - x.Age;
        }
    }
    internal class AscendingAgeSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return x.Age - y.Age;
        }
    }
    internal class AscendingCitytSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return x.City.CompareTo(y.City);
        }
    }
    internal class DescendingCitySorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return y.City.CompareTo(x.City);
        }
    }
    internal class AscendingNameSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }
}
