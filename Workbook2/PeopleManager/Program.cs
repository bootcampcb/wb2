﻿// People Manager
using PeopleManager;

List<Person> people = new List<Person>();

people.Add(new Person("Chuck", 35, "1234 SomeWhere", "HERE", "MI", "48167"));
people.Add(new Person("Mark", 30, "5678 SomeWhere", "THERE", "MI", "48150"));
people.Add(new Person("Dan", 29, "9012 SomeWhere", "EVERYWHERE", "MI", "48375"));
people.Add(new Person("Sean", 28, "3456 SomeWhere", "ELSEWHERE", "MI", "48012"));
people.Add(new Worker("Maaike", 32, "1357 there", "ASFE", "NY", "00123","Consultant", 40000M));

Console.WriteLine($"List of {people.Count} people");
foreach (Person person in people)
{
    person.Display();
}

Console.WriteLine("List people sorted by age");
people.Sort(new AscendingAgeSorter());
foreach (Person person in people)
{
    person.Display();
}
Console.WriteLine("List people sorted descending by age");
people.Sort(new DescendingAgeSorter());
foreach (Person person in people)
{
    person.Display();
}

Console.WriteLine("List people sorted city");
people.Sort(new AscendingCitytSorter());
foreach (Person person in people)
{
    person.Display();
}

Console.WriteLine("List people sorted descending by city");
people[3].Move("5555 somewhere else", "Somewhere", "MI", "49001");
people.Sort(new DescendingCitySorter());
foreach (Person person in people)
{
    person.Display();
}

Console.WriteLine("List people sorted name");
people.Sort(new AscendingNameSorter());
foreach (Person person in people)
{
    person.Display();
}